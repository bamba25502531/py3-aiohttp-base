# py3-aiohttp-base
> An opinionated docker image intended for writing web services using python3 and aiohttp.

## Usage

### For building python applications

Use this like any other Docker image:
```dockerfile
FROM registry.gitlab.com/aporia/py3-aiohttp-base
COPY my-python-code .
```

### As a fixed environment for package development

Sharing a docker image between developers is a lot easier and less error-prone than sharing a virtual environment. Thus,
if you can use a remote interpreter in your IDE (f.e. using [PyCharm][1] or [IntelliJ][2]), this will ensure a consistent
environment for all your developers that gets upgraded everytime.

## Versioning

The image is versioned using [semantic versioning][3]. But what does this mean in practice for this image?

Since this is essentially a meta-package then this implies a major version update whenever an included python package receives a major version update (except otherwise noted).
Similarly, the minor version is increased whenever an included package's minor version gets increased. The patch version is increased whenever a bundled package receives a patch revision, or when the configuration for the bundled development dependencies gets updated.

The notable exception to major version updates is the `certifi` package. An update to `certifi` will generally yield a patch revision update for this image.

## What makes this an opinionated image?

Of course, since this is software and intended as a "base environment image" then a lot of the opinions can easily be changed by simply adjusting your own builds.

Nevertheless you are probably curious as to what you find here.

First of all, it's the selection of module py.test, pylint and aiohttp are not everyone's choice for their respective tasks.
Secondly, the configuration for the development dependencies is opinionated. Not everyone enjoys to break tests at the first error, or to produce their coverage/lint/test reports in the given format.
Lastly, this image assumes that you want to have your CI infrastructure simply run along the following lines:
```bash
$ docker build -t myimage:mytag .  # build the image           # build the image which contains the full runtime environment
$ docker run -d --rm --name myimage_ci_run myimage:mytag bash  # run bash in the background so that we can ...
$ docker cp myimage_ci_run:/*.xml .                            # copy the test/lint/coverage results out of the build
$ docker stop myimage_ci_run                                   # stop the container so that we clean up
$ docker push myimage:mytag                                    # now go and push it to your registry
```


[1]: https://www.jetbrains.com/help/pycharm/configuring-remote-python-interpreters.html
[2]: https://intellij-support.jetbrains.com/hc/en-us/community/posts/206885305-Configuring-Remote-Python-Interpreter
[3]: https://semver.org