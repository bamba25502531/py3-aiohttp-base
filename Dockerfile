FROM python:3.6-slim
RUN apt-get update -y && apt-get install -y build-essential --no-install-recommends
RUN pip install --no-cache-dir --upgrade pip==9.0.3
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY distutils.cfg /usr/local/lib/python3.6/distutils/